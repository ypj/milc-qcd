/***************** control.c *****************************************/

/* Main procedure for clover spectrosopy      */
/* MIMD version 7 */

/* This version computes propagators for clover fermions on a supplied
   background field config and ties them together according to the
   input parameters. */

/* Modifications ...
   
   8/8/98  Rearranged loop so hadron propagators are written as soon
           as they are calculated. C.D.
   8/10/96 Installed new propagator IO and added timing C.D.
   5/29/07 Generalized the algorithm C.D.
   3/8/17  [garfield] Remove CLOV_LEAN. Note that this code is intended to 
                      avoid propagator I/O anyway.
 */

#define CONTROL
#include "cl_inv_includes.h"
#include <string.h>
#ifdef HAVE_QUDA
#include <quda_milc_interface.h>
#endif

int main(int argc, char *argv[])
{
  int prompt;
  int i, j, k, iq0, iq1;
  double starttime, endtime;
#ifdef PRTIME
  double dtime;
#endif
  wilson_prop_field *prop[MAX_PROP] = {NULL,}; // garfield [modify] init
  wilson_prop_field *quark[MAX_QK];
  
  initialize_machine(&argc,&argv);

  /* Remap standard I/O */
  if(remap_stdio_from_args(argc, argv) == 1)terminate(1);
  
  g_sync();
  
  starttime=dclock();
    
  /* set up */
  STARTTIME;
  prompt = setup();
  ENDTIME("setup");

  /* loop over input sets */

  while( readin(prompt) == 0){
    if( param.startflag == CONTINUE_TO_QUARK ||
        param.startflag == FREEPROPMEM){
      // garfield [add] skip source and propagator
    } else {
      node0_printf("param.startflag=%d\n",param.startflag);

      if(prompt == 2)continue;
      
      total_iters=0;
      
#ifdef HISQ_SVD_COUNTER
      hisq_svd_counter = 0;
#endif
    
      /**************************************************************/
      /* Set up gauge field */
      
      if( param.fixflag == COULOMB_GAUGE_FIX)
      {
        node0_printf("Fixing to Coulomb gauge\n");

        STARTTIME;
        gaugefix(TUP,(Real)1.5,500,GAUGE_FIX_TOL);
        ENDTIME("gauge fix");

        /* (Re)construct APE smeared links after gauge fixing.  
           No KS phases here! */
        destroy_ape_links_3D(ape_links);
        ape_links = ape_smear_3D( param.staple_weight, param.ape_iter );

        invalidate_this_clov(gen_clov);
      }
      else
        node0_printf("COULOMB GAUGE FIXING SKIPPED.\n");
      
      /* save lattice if requested */
      if( param.saveflag != FORGET ){
        savelat_p = save_lattice( param.saveflag, param.savefile, 
                                  param.stringLFN );
      } else {
        savelat_p = NULL;
      }

      node0_printf("END OF HEADER\n");
      
      /**************************************************************/


      /* Loop over the propagators */

      STARTTIME;
      for(i=0; i<param.num_prop; i++){
        
        /**************************************************************/
        /* Read and/or generate quark propagator */

        if(param.prop_type[i] == CLOVER_TYPE)
        {
          int ncolor = convert_ksource_to_color(param.src_qs[i].nsource);
          
          prop[i] = create_wp_field(ncolor);
            
          node0_printf("Generate Dirac propagator\n");
          node0_printf("Kappa= %g source %s residue= %g rel= %g\n",
                       (double)param.dcp[i].Kappa,
                       param.src_qs[i].descrp,
                       (double)param.qic[i].resid,
                       (double)param.qic[i].relresid);
          
          /* For clover_info */
          wqstmp = param.src_qs[i];
          dcptmp = param.dcp[i];
          
          // garfield [add]
					if (param.src_qs[i].type == COHERENT_DIRAC_PROPAGATOR) {
            if (prop[param.src_qs[i].src_prop] == NULL) {
		          node0_printf("ERROR: coherent dirac propagator must precede sequencial propagator\n");
		          terminate(1);
	          } else {
						  param.src_qs[i].wp_src = prop[param.src_qs[i].src_prop];
            }
					}

          total_iters += get_wprop_to_wp_field(param.prop_type[i],
                       param.startflag_w[i], 
                       param.startfile_w[i], 
                       param.saveflag_w[i], 
                       param.savefile_w[i],
                       prop[i], 
                       &param.src_qs[i],
                       &param.qic[i], 
                       (void *)&param.dcp[i],
                       param.bdry_phase[i],
                       param.coord_origin,
                       param.check[i]);
        }
  
        /* ------------------------------------------- */
        else if(param.prop_type[i] == IFLA_TYPE) 
        {
          int ncolor = convert_ksource_to_color(param.src_qs[i].nsource);

          prop[i] = create_wp_field(ncolor);
            
          node0_printf("Generate Dirac IFLA propagator\n");
          node0_printf("Kappa= %g source %s residue= %g rel= %g\n",
                       (double)param.nap[i].kapifla,
                       param.src_qs[i].descrp,
                       (double)param.qic[i].resid,
                       (double)param.qic[i].relresid);
         
          /* For clover_info */
          wqstmp = param.src_qs[i];
          naptmp = param.nap[i];
			   
          // garfield [add]
					if (param.src_qs[i].type == COHERENT_DIRAC_PROPAGATOR) {
            if (prop[param.src_qs[i].src_prop] == NULL) {
		          node0_printf("ERROR: coherent dirac propagator must precede sequencial propagator\n");
		          terminate(1);
	          } else {
						  param.src_qs[i].wp_src = prop[param.src_qs[i].src_prop];
            }
					}
          
          total_iters += get_wprop_to_wp_field(param.prop_type[i],
                       param.startflag_w[i], 
                       param.startfile_w[i], 
                       param.saveflag_w[i], 
                       param.savefile_w[i],
                       prop[i],
                       &param.src_qs[i], 
                       &param.qic[i], 
                       (void *)&param.nap[i],
                       param.bdry_phase[i],
                       param.coord_origin,
                       param.check[i]);
        }
        else if(param.prop_type[i] == COHERENT_DIRAC_TYPE) 
        {
          int ncolor = convert_ksource_to_color(param.src_qs[param.prop_concat_list[i][0]].nsource);
          
          prop[i] = create_wp_field(ncolor);
            
          node0_printf("Build Coherent Dirac propagators\n");
          
          /* For clover_info */
          wqstmp = param.src_qs[i];
          dcptmp = param.dcp[i];
          naptmp = param.nap[i];

          int base_prop;
          int t_sep;
          int ncc;
          int tmpsaveflag;
          
					t_sep = param.t_sep_coherent[i];
          ncc = nt/t_sep;

          for ( int p=0; p<ncc; p++ ) {
            if (p == ncc-1) { tmpsaveflag = param.saveflag_w[i]; }
            else { tmpsaveflag = FORGET; }

            base_prop = param.prop_concat_list[i][p];
            concat_wprop_to_wprop(param.startflag_w[i], 
                                  param.startfile_w[i],
                                  tmpsaveflag, 
                                  param.savefile_w[i],
                                  prop[i],
                                  prop[base_prop],
                                  &param.src_qs[base_prop],
                                  t_sep);
          }
        }
        else if( (param.prop_type[i] == KS_TYPE || 
                  param.prop_type[i] == KS0_TYPE) ) /* KS_TYPE */
        {
          prop[i] = create_wp_field(param.src_qs[i].ncolor);

          node0_printf("Mass= %g source %s residue= %g rel= %g\n",
                       (double)param.ksp[i].mass,
                       param.src_qs[i].descrp,
                       (double)param.qic[i].resid,
                       (double)param.qic[i].relresid);
          
          total_iters += get_ksprop_to_wp_field(param.startflag_ks[i], 
                  param.startfile_ks[i], 
                  param.saveflag_ks[i], 
                  param.savefile_ks[i],
                  prop[i], 
                  &param.src_qs[i],
                  &param.qic[i], 
                  &param.ksp[i],
                  param.bdry_phase[i],
                  param.coord_origin,
                  param.t_sep_coherent[i], // garfield [add]
                  param.check[i],
                  param.prop_type[i] == KS0_TYPE);
        }
        else /* KS4_TYPE */
        {
          prop[i] = create_wp_field(param.src_qs[i].ncolor);
      
          node0_printf("Mass= %g source %s residue= %g rel= %g\n",
                       (double)param.ksp[i].mass,
                       param.src_qs[i].descrp,
                       (double)param.qic[i].resid,
                       (double)param.qic[i].relresid);
          
          total_iters += get_ksprop4_to_wp_field(param.startflag_w[i], 
                   param.startfile_w[i], 
                   param.saveflag_w[i], 
                   param.savefile_w[i],
                   prop[i], 
                   &param.src_qs[i],
                   &param.qic[i], 
                   &param.ksp[i],
                   param.bdry_phase[i],
                   param.coord_origin,
                   param.check[i]);
        }
        
      } /* propagators */
      ENDTIME("compute propagators");
    }

    /*****************************************************************/
    /* Complete the quark propagators by applying the sink operators
       to either the raw propagator or by building on an existing quark
       propagator */
    
    STARTTIME;
    for(j=0; j<param.num_qk; j++){
      i = param.prop_for_qk[j];

      if(param.parent_type[j] == PROP_TYPE){
        /* Apply sink operator quark[j] <- Op[j] prop[i] */
        quark[j] = create_wp_field_copy(prop[i]);
        wp_sink_op(&param.snk_qs_op[j], quark[j]);

        /* Can we delete any props and quarks now? */
        /* If nothing later depends on a prop or quark, free it up. */
        if ( param.startflag == FREEPROPMEM ) 
        { // garfield [add]
          for(i = 0; i < param.num_prop; i++)
            if( prop[i]->swv[0] != NULL  &&  param.prop_dep_qkno[i] < j ){
              free_wp_field(prop[i]);
              node0_printf("free prop[%d]\n",i);
            }
        }

        for(i = 0; i < j; i++)
          if( quark[i]->swv[0] != NULL  &&  param.quark_dep_qkno[i] < j ){
            free_wp_field(quark[i]);
            node0_printf("free quark[%d]\n",i);
          }
      }
      else if(param.parent_type[j] == QUARK_TYPE) {
        /* Apply sink operator quark[j] <- Op[j] quark[i] */
        quark[j] = create_wp_field_copy(quark[i]);
        wp_sink_op(&param.snk_qs_op[j], quark[j]);
      } else { /* COMBO_TYPE */
        int k;
        int nc = quark[param.combo_qk_index[j][0]]->nc;
        /* Create a zero field */
        quark[j] = create_wp_field(nc);
        /* Compute the requested linear combination */
        for(k = 0; k < param.num_combo[j]; k++){
          wilson_prop_field *q = quark[param.combo_qk_index[j][k]];
          if(nc != q->nc){
            node0_printf("Error: Attempting to combine an inconsistent number of colors: %d != %d\n",nc, q->nc);
            terminate(1);
          }
          scalar_mult_add_wprop_field(quark[j], 
                                      q, param.combo_coeff[j][k], 
                                      quark[j]);
        }
      }
  
      /* Save the resulting quark[j] if requested */
      dump_wprop_from_wp_field( param.saveflag_q[j], param.savetype_q[j],
                                param.savefile_q[j], quark[j]);
    }

    /* Now destroy all remaining propagator fields */
    if( param.startflag == FREEPROPMEM ) 
    { // garfield [add]
      for(i = 0; i < param.num_prop; i++){
        if(prop[i] != NULL)node0_printf("destroy prop[%d]\n",i);
        destroy_wp_field(prop[i]);
        prop[i] = NULL;
      }
    }
    ENDTIME("generate quarks");
    
    /****************************************************************/
    /* Compute the meson propagators */

    STARTTIME;
    for(i = 0; i < param.num_pair; i++){

      /* Index for the quarks making up this meson */
      iq0 = param.qkpair[i][0];
      iq1 = param.qkpair[i][1];

      node0_printf("Mesons for quarks %d and %d\n",iq0,iq1);

      /* Tie together to generate hadron spectrum */
      spectrum_cl(quark[iq0], quark[iq1], i);

      /* Remember, in case we need to free memory */
    }
    ENDTIME("tie hadron correlators");

    node0_printf("RUNNING COMPLETED\n");
    endtime=dclock();

    node0_printf("Time = %e seconds\n",(double)(endtime-starttime));
    node0_printf("total_iters = %d\n",total_iters);
#ifdef HISQ_SVD_COUNTER
    node0_printf("hisq_svd_counter = %d\n",hisq_svd_counter);
#endif
    fflush(stdout);

    for(i = 0; i < param.num_qk; i++){
      if(quark[i] != NULL)node0_printf("destroy quark[%d]\n",i);
      destroy_wp_field(quark[i]);
      quark[i] = NULL;
    }

    destroy_ape_links_3D(ape_links);

    /* Destroy fermion links (possibly created in make_prop()) */

#if FERM_ACTION == HISQ
    destroy_fermion_links_hisq(fn_links);
#else
    destroy_fermion_links(fn_links);
#endif
    fn_links = NULL;

  } /* readin(prompt) */

#ifdef HAVE_QUDA
  qudaFinalize();
#endif

  return 0;
}
